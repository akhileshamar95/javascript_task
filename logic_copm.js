
 5 < 4
// = false

> 5 < 5
// = false
// Compare with the `<` examples.

> 4 > 5
// = false

> 5 > 4
// = true

> 5 > 5
// = false

> 4 <= 5
// = true

> 5 <= 4
// = false

> 5 <= 5
// = true
> 4 >= 5
// = false

> 5 >= 4
// = true

> 5 >= 5
// = true

> 5 === 5
// = true

> 5 === 4
// = false

> 5 === '5'
// = false
> 5 !== 5
// = false

> 5 !== 4
// = true

> 5 !== '5'
// = true


> 5 == 5
// = true

> 5 == 4
// = false

> 5 == '5'
// = true

> 5 != 5
// = false

> 5 != 4
// = true

> 5 != '5'
// = false

> (4 === 4) && (5 === 5)
// = true

> (4 === 4) && (5 === 6)
// = false

> (4 === 5) && (5 === 5)
// = false

> (4 === 5) && (5 === 6)
// = false

> (4 === 4) || (5 === 5)
// = true

> (4 === 4) || (5 === 6)
// = true

> (4 === 5) || (5 === 5)
// = true

> (4 === 5) || (5 === 6)
// = false

> !true
// = false

> !false
// = true

> !(4 === 4)
// = false

> !(4 !== 4)
// = true
> !!3
// = true

> !!''
// = false

