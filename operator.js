/**
 * operator:-
 * 1.arithmetic operator like that- +,-,*,/,%,++,--;
 * 2.assignment operator like that - +=.-= x+=2;....;
 * 3.logic operator- &&,||, !;
 * 4.comperision operator ->,<,>=,<=,===,!==,
 * 
 */
// arithmetic
// var x,y,z;
// x=11;
// y=5;
// z=x+y;
// console.log(z);//15
// z=x-y;
// console.log(z);//5
// z=x*y;
// console.log(z);//50
// z=x/y;
// console.log(z);//2
// z=x%y;
// console.log(z);

// assignment
// x+=2;
// console.log(x);
// x-=1;
// console.log(x);
// x*=2;
// console.log(x);
// x/=2;
// console.log(x);
// x%=2;
// console.log(x);

// logic operator

age=24;
if (!(age > 18)) {
    console.log("sorry bhai , you are not allow for vote");
}
else if (age > 18 && age <= 35) {
    console.log("Young Man");
}
else if (age == 36 || age <= 60) {
    console.log("You are Middle Age");
}
else {
    console.log("You are too Old");
}